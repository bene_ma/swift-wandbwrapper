import Foundation
import TensorFlow
import PythonKit

public protocol TensorWithShape {
    var shape: TensorShape { get }
}
//
//extension Dictionary: PythonConvertible where Self.Key: PythonConvertible, Self.Value: PythonConvertible {
//
//    public var pythonObject: PythonObject {
//        return self.mapValues({ $0.pythonObject }).pythonObject
//    }
//}

public enum WandBUtilities {
    
    /// returns a `Dictionary` of parameters of a TensorFlow model or optimizer.
    /// this function recursively goes through all variables of an object and print its primitive value
    /// each primitive value will be stored with the path to the variable, separated by '.' (example: output["value1.subvalue"] = 0.4)
    /// - Parameters:
    ///   - of: the object to retrieve all parameters from
    ///   - ignoreTypes:(optional) a `List` of type names as `String`'s to ignore while parsing. For example ["Int"] will ignore Int parameters
    ///   - key: (optional)  the first key to use
    public static func parameters(of element: Any, ignoreTypes: [String] = [], key: String = "root") -> RecursiveDict<String, PythonConvertible> {
        var params = RecursiveDict<String, PythonConvertible>()

        /* If the current element, we are looking at is an array of PythonConvertible,
         * store each array entry as key[index] = value by converting value into a PythonConvertible type
         */
        if element is [PythonConvertible] {
            let array = (element as! [PythonConvertible])
            params[key]["type"] = "TensorShape"
            params[key]["value"] = "\(array)"
            // enumerated() converts the array into (index, value) pairs, iterate through them all with forEach
            array.enumerated().forEach({ (index, value) in
                params[key]["\(index)"]["type"] = String("\(type(of: value))")
                params[key]["\(index)"]["value"] = value as PythonConvertible
            })
            return params
        }
        /* A Mirror gives us an array of all sub-parameters that an element has.
         * Each child of the mirror reflects a variable lable and value.
         */
        let mirror = Mirror(reflecting: element)
        params[key]["type"] = String(describing: mirror.subjectType)
        mirror.children.forEach({ child in
            // check if the current child has a label. Continue with the next one if not by returning
            guard var childLabel = child.label else { return }
            childLabel.removeAll(where: { $0 == "." })
            // a String which tells us the type of the child (String, Int, Float, Tensor<Float>...)
            let typeOfChild: String = String("\(type(of: child.value))")
            // We can filter out types to clean up the output
            guard !childLabel.isEmpty && !ignoreTypes.contains(typeOfChild) else { return }
            
            // We decide how to store the value depending on the type
            switch child.value {
            case is PythonConvertible:
                // The variable is a PythonConvertible! We can just cast it and append it to the dictionary
                params[key][childLabel]["type"] = typeOfChild
                params[key][childLabel]["value"] = (child.value as! PythonConvertible)
            case is Tensor<Float>:
                // A Tensor is a special case. I am not interested in the specific values of the tensor.
                // Instead I want to store the dimensions of the Tensor to keep track of the hyperparameters
                // we call parameters(of: ) recursively and merge the results with our current params
                let tensorParams = parameters(of: (child.value as! Tensor<Float>).shape.dimensions, key: "shape")
                params[key][childLabel] = tensorParams
            default:
                // If it is any other type try to recursively append the parameters of that type using the childKey
                let childParams = parameters(of: child.value, ignoreTypes: ignoreTypes, key: childLabel)
                let dict: RecursiveDict<String, PythonConvertible> = childParams[childLabel]
                params[key][childLabel] = dict
            }
        })
        return params
    }
    
    
    public static func generateParameters(model: Any, optimizer: Any) -> PythonObject
    {
        let modelParameters = WandBUtilities.parameters(of: model, key: "model")
        let optimizerParameters = WandBUtilities.parameters(of: optimizer, ignoreTypes: ["TangentVector"], key: "optimizer")
        
        var params: RecursiveDict<String, PythonObject> = RecursiveDict<String, PythonObject>()
        modelParameters.forEach({ params[$0.key] = $0.value.pythonObject })
        optimizerParameters.forEach({ params[$0.key] = $0.value.pythonObject })
        
        
        return params.pythonObject
    }
    
    public static func generateTags(model: Any, optimizer: Any) -> [String]
    {
        let modelName = "\(type(of: model))"
        let optimizerName = "\(type(of: optimizer))".components(separatedBy: "<")[0]
        var tags = [optimizerName, modelName]
        tags += Mirror(reflecting: model).children.map({ "\(type(of: $0.value))".replacingOccurrences(of: "<Float>", with: "") })
        return tags;
    }
}
