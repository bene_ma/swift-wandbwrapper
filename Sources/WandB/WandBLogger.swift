import Foundation
import TensorFlow
import PythonKit

import TrainingLoop

public typealias WandBLoggingCallback<L: TrainingLoopProtocol> = (
    _ loop: inout L, _ event: TrainingLoopEvent, _ logger: WandBLogger
) throws -> Void

public struct WandBLogger
{
    public let wandb: WandB
    public let project: String
    
    public var parameters: [String: PythonConvertible] = [:]
    public var tags: Set<String> = []
    
    // dynamic parameters (changing during callabck execution)
    public var runner: PythonObject? = nil
    public var step: Int = 0
    public var lastSync: Date = Date()

    init(wandb: WandB,
         project: String,
         parameters: [String: PythonConvertible] = [:],
         tags: [String] = []
    ) {
        self.wandb = wandb
        self.project = project
        self.parameters = parameters
        self.tags = Set(tags)
    }
    
    public func log<L: TrainingLoopProtocol>(
        model: L.Opt.Model,
        optimizer: L.Opt,
        customLog: WandBLoggingCallback<L>? = nil
    ) -> TrainingLoopCallback<L> where L.Opt.Scalar: PythonConvertible, L.Input: TensorProtocol, L.Output: TensorProtocol, L.Target: TensorProtocol
    {
        // capture self
        var logger: WandBLogger = self
    
        return { (loop, event) throws -> Void in
            switch event {
            // .fitStart: The start of a training loop fit() call
            case .fitStart:
                let batchSize: Int? = loop.validation.first?.data.shape[0]
                guard logger.runner == nil else { break }

                // WandB config Parameters
                let parametersPython = WandBUtilities.generateParameters(model: model, optimizer: optimizer)
                logger.parameters.forEach({ parametersPython[$0.key] = $0.value.pythonObject })
                if let epochCount = loop.epochCount { parametersPython["epochs"] = epochCount.pythonObject }
                if let batchSize = batchSize { parametersPython["batchSize"] = batchSize.pythonObject }
                
                // WandB Tags
                logger.tags = logger.tags.union(Set(WandBUtilities.generateTags(model: model, optimizer: optimizer)))
                
                // WandB init
                // we need to call wandb.`init` here, since init is a reserved word in swift and we are calling a python object function, not an initializer
                logger.runner = wandb.pythonWrapper.`init`(
                    project: self.project,
                    tags: Array(logger.tags),
                    config: parametersPython,
                    resume: logger.runner != nil ? logger.runner!.id() : false
                )
                
                print("***WandB Logger started!***")
                
            // .fitEnd: The end of a training loop fit() call
            case .fitEnd:
                if let wandbRunner = logger.runner {
                    // call customLog first
                    if let customLog = customLog {
                        try customLog(&loop, event, logger)
                    }
                    
                    wandbRunner.finish()
                    print("***WandB Logger stopped!***")
                }
                // return so that we won't call customLog twice
                return
                
            // .batchEnd: The start of a batch, can be in training or in validation stage
            case .batchStart:
                // increment at the start of each batch
                logger.step += 1
            // .batchEnd: The end of a batch, can be in training or in validation stage
            case .batchEnd:
                // validation progress will be logged once every epoch, while training is logged every batch
                guard Context.local.learningPhase == .training else { break }
                guard let wandbRunner = logger.runner else { print("W&B Error"); break }
                guard let epochIndex = loop.epochIndex,
                      let batchIndex = loop.batchIndex,
                      let batchCount = loop.batchCount,
                      let lastStepLoss = loop.lastStepLoss
                else { print("Data error in batch end logging!"); break}
                
                // only send once every 2 seconds
                let shouldSend: Bool = logger.lastSync.timeIntervalSinceNow < -5
                if (shouldSend) { logger.lastSync = Date()}
                
                let epoch: Float = Float(epochIndex) + (Float(batchIndex) / Float(batchCount))
                // log all stats
                let loss: Float = lastStepLoss.scalars[0]
                wandbRunner.log(["step_loss": loss], step: logger.step, commit: false)
                wandbRunner.log(["learning_rate": loop.optimizer.learningRate], step: logger.step, commit: false)
                wandbRunner.log(["batch": batchIndex], step: logger.step, commit: false)
                wandbRunner.log(["epoch": epoch], step: logger.step, commit: shouldSend)
                
            // .trainingEnd: The end of a training phase
            case .trainingEnd:
                guard let wandbRunner = logger.runner else { print("W&B Error"); break }
                guard let stats = loop.lastStatsLog else { break }
                // log all stats
                for (name, value) in stats {
                    wandbRunner.log(["train_" + name: value], step: logger.step, commit: false)
                }
            // .validationEnd: The end of a validation phase
            case .validationEnd:
                guard let wandbRunner = logger.runner else { print("WandB Error"); break }
                guard let epochIndex = loop.epochIndex, let stats = loop.lastStatsLog
                else { print("Data error in batch end logging!"); break }
                
                let epoch: Float = Float(epochIndex + 1)
                for (name, value) in stats {
                    wandbRunner.log(["valid_" + name: value], step: logger.step, commit: false)
                }
                wandbRunner.log(["epoch": epoch], step: logger.step, commit: true)
                
            default:
                break;
            }
            // If a customLog function is passed and we have wandbRunner instance, call it!
            if let customLog = customLog {
                try customLog(&loop, event, logger)
            }
            
        }
    }
        
}
