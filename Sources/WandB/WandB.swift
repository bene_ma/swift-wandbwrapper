import Foundation
import TensorFlow
import PythonKit

import TrainingLoop

public protocol WandBDataset {
    associatedtype Training : Sequence where Self.Training.Element : Collection
    associatedtype Validation : Collection
}
    
public struct WandB {
    public let pythonWrapper: PythonObject
    
    public init(apiKey: String = "") {
        self.pythonWrapper = Python.import("wandb")
        if apiKey.count > 0 {
            self.pythonWrapper.login(key: apiKey)
        }
    }
    
    public func createLogger (
        forProject: String,
        parameters: [String: PythonConvertible] = [:],
        tags: [String] = []
    ) -> WandBLogger {
        return WandBLogger(wandb: self, project: forProject, parameters: parameters, tags: tags)
    }
}
