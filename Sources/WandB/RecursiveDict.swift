import PythonKit

public enum RecursiveDictValue<KeyType: Hashable, ValueType> {
    case Value(ValueType)
    case Dict(RecursiveDict<KeyType, ValueType>)
}

extension RecursiveDictValue: PythonConvertible where KeyType == String{
    public var pythonObject: PythonObject {
        switch (self) {
        case let .Value(value) where value is PythonConvertible:
            return (value as! PythonConvertible).pythonObject
        case let .Dict(dict):
            return dict.pythonObject
        default:
            return "<err>"
        }
    }
}

public struct RecursiveDict<KeyType: Hashable, ValueType> {
    public typealias DictValueType = RecursiveDictValue<KeyType, ValueType>
    typealias OwnType = RecursiveDict<KeyType, ValueType>

    public typealias DictType = [KeyType: DictValueType]
    
    private var dict: DictType

    public init(_ dict: DictType = [:]) {
        self.dict = dict
    }
    
    public init(_ dict: [KeyType: ValueType]) {
        self.dict = dict.mapValues({ .Value($0) })
    }

    // this ensures that we can safely chain subscripts
    subscript(key: KeyType) -> OwnType {
        get {
            switch dict[key] {
            case let .Dict(dict)?:
                return dict
            default:
                return RecursiveDict<KeyType, ValueType>()
            }
        }

        set(newValue) {
            dict[key] = .Dict(newValue)
        }
    }

    subscript(key: KeyType) -> ValueType? {
        get {
            switch dict[key] {
            case let .Value(value)?:
                return value
            default:
                return nil
            }
        }

        set(newValue) {
            if let newValue = newValue {
                dict[key] = DictValueType.Value(newValue)
            } else {
                dict[key] = nil
            }
        }
    }
}


extension RecursiveDict: Collection {
    public typealias Index = DictType.Index
    public typealias Iterator = DictType.Iterator
    
    public __consuming func makeIterator() -> DictType.Iterator {
        return dict.makeIterator()
    }
    
    public func index(after i: Index) -> Index{
        return dict.index(after: i)
    }
    
    public subscript(position: Index) -> Iterator.Element {
        get { dict[position] }
    }
    
    public var startIndex: Index {
        return dict.startIndex
    }
    public var endIndex:  Index {
        return dict.endIndex
    }
    
}

extension RecursiveDict: CustomStringConvertible {
    public var description: String {
        self.dict.map({ (key, value) -> String in
            switch (value) {
            case let .Value(value):
                return "\"\(key)\": \(value), "
            case let .Dict(value):
                return "{\"\(key)\": {\(value)},"
            }
        }).reduce("", +)
    }
}

extension RecursiveDict: PythonConvertible where KeyType == String {
    public var pythonObject: PythonObject {
        let pythonDict: PythonObject = [:]
        self.dict.sorted(by: { $0.key == "type" || $0.key < $1.key }) // some custom sorting here.
            .forEach({ (key, value) in
            switch (value) {
            case let .Value(value) where value is PythonConvertible:
                pythonDict[key] = (value as! PythonConvertible).pythonObject
            case let .Dict(value):
                return pythonDict[key] = value.pythonObject
            default:
                print("Er: Invalid key: \(type(of: value)), \(value)")
            }
        })
        return pythonDict
    }
}
