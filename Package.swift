// swift-tools-version:5.3
import PackageDescription

let package = Package(
    name: "swift-wandb",
    platforms: [
        .macOS(.v10_13)
    ],
    products: [
        .library(
            name: "WandB", targets: ["WandB"]),
    ],
    dependencies: [
        .package(url: "https://github.com/pvieito/PythonKit.git", .branch("master")),
        .package(url: "https://github.com/tensorflow/swift-models", .branch("main")),
    ],
    targets: [
        .target(
            name: "WandB",
            dependencies: [
                .product(name: "TrainingLoop", package: "swift-models"),
            ]
        ),
        .testTarget(
            name: "WandBTests",
            dependencies: [
                "WandB",
                .product(name: "Datasets", package: "swift-models"),
                .product(name: "ImageClassificationModels", package: "swift-models"),
            ]
        ),
    ]
)
