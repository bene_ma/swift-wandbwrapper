import XCTest

import swift_wandbTests

var tests = [XCTestCaseEntry]()
tests += WandBTests.allTests()
XCTMain(tests)
