import XCTest

import TensorFlow
import TrainingLoop

import Datasets
import ImageClassificationModels

import PythonKit

@testable import WandB

final class WandBTests: XCTestCase {
    
    func testParameterExtraction() {
        let model = LeNet()
        let optimizer = SGD(for: model)
        
        let params = WandBUtilities.generateParameters(model: model, optimizer: optimizer)
        let json = Python.import("json")
        print(json.dumps(params.pythonObject, indent: 2))
    }
    
    func testLogging() {
        let wandb = WandB()
        let wandbLogger = wandb.createLogger(forProject: "unittests")

        let dataset = MNIST(batchSize: 256)

        var model = LeNet()
        let optimizer = SGD(for: model)

        var trainingLoop = TrainingLoop(
          training: dataset.training,
          validation: dataset.validation,
          optimizer: optimizer,
          lossFunction: softmaxCrossEntropy,
          metrics: [ .accuracy ],
          callbacks: [
            wandbLogger.log(model: model, optimizer: optimizer)
          ]
        )

        try? trainingLoop.fit(&model, epochs: 1)

//        print("Testing resuming")
//
//        try? trainingLoop.fit(&model, epochs: 1)
    }

    static var allTests = [
        ("testParameterExtraction", testParameterExtraction),
        ("testLogging", testLogging),
    ]
}
